=== Plugin Name ===
Contributors: wp-making
Donate link: 
Tags: REST, custom, route, endpoint, custom endpoint, custom route, API
Requires at least: 4.4
Tested up to: 5.0
Stable tag: 4.4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Building custom endpoints for WP REST API made easy.

== Description ==

Have you ever thought in the possibility of adding custom endpoints for WP REST API without touching a single line of PHP? That can be easily done with Rest Routes! It lets you build custom endpoints along with custom filters and output. All from inside of the WordPress dashboard side.

You can create an unlimited number of custom routes for your site using a very friendly interface. All you have to do is:

*   Give the custom endpoint/route a Name
*   Customize the filter as you wish
*   Define what will be the route's output
*   Enjoy the WP REST API custom endpoint you just built without touching a single line of code

A couple of available filters for your custom route:

*   Default and Custom Post Types
*   Default Taxonomies
*   Post Status
*   Post Parent
*   Post ID
*   Post Author
*   Ordering
*   Limit & Offset

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/rest-routes` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress

== Screenshots ==

1. List of custom endpoints
2. Choosing filters to a custom endpoint
3. Defining the output of the custom endpoint
4. The JSON output of your custom endpoint

== Changelog ==

= 1.7.0 =

- Fixed version number

= 1.6.0 =

- Fixed version number

= 1.5.0 =

- Fixed some PHP notices

= 1.4.0 =

- Fixed bugs

= 1.3.0 =

- Fixed bugs

= 1.2.0 =

- Fixed a bug when creating a route with only one field in output

= 1.1.0 =

- Fixed a some warning messages
- Added support to custom post types

= 1.0.0 =
First release.