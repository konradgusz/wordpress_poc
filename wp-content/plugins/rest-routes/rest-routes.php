<?php
/*
Plugin Name: Rest Routes - Custom Endpoints for WP REST API
Plugin URI: https://wordpress.org/plugins/custom-wp-rest-api/
Description: Building custom endpoints for WP REST API made easy.
Author: WP Making
Version: 1.7.0
*/

define( 'WPRR_VERSION', '1.7.0' );
define( 'WPRR_INC_PATH', dirname( __FILE__ ) . '/inc' );
define( 'WPRR_PATH', dirname( __FILE__ ) );
define( 'WPRR_FOLDER', basename(WPRR_PATH) );
define( 'WPRR_URL', plugins_url() . '/'. WPRR_FOLDER );
define( 'TEXTDOMAIN', 'rest-routes' );

include_once( WPRR_INC_PATH . '/' . 'wprr-restroutes.class.php' );

$RestRoutes = new RestRoutes();

add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'my_plugin_action_links' );

function my_plugin_action_links( $links ) {
	$links = array_merge( array(
		'<a target="_blank" style="color: #e57606; font-weight:bold" href="https://docs.google.com/forms/d/e/1FAIpQLSeYryPJ4nRDMVTy_M0sBMPNjhXPEQtsJUD85Ko6NtSECuG41A/viewform?usp=sf_link&hl=en">' . __( 'Get Pro', 'textdomain' ) . '</a>'
	), $links );
	return $links;
}