<div class="wrap">
	<h2>Mega Blocks Settings</h2>
	<p>Here you can enable or disable blocks.</p>
	<form id="mbg-settings-form">
	<input type="hidden" name="action" value="mbg_save_settings">
	<?php
		$saved = get_option( 'mbg_blocks_settings' );
		if (isset($saved['action']) && $saved['action'] == 'mbg_save_settings') {
			$all_checked = false;
			$s_arr = (isset($saved['block'])) ? $saved['block'] : array();
		} else {
			$all_checked = true;
			$s_arr = array();
		}

		$block_data = array(
			array(
				'title' => __( 'Icon', 'mega-blocks-gutenberg' ),
				'key' => 'icon'
			),
			array(
				'title' => __( 'List Group', 'mega-blocks-gutenberg' ),
				'key' => 'list-group'
			),
			array(
				'title' => __( 'Pricing Table', 'mega-blocks-gutenberg' ),
				'key' => 'pricing-table'
			),
			array(
				'title' => __( 'Image Hover', 'mega-blocks-gutenberg' ),
				'key' => 'image-hover'
			),
			array(
				'title' => __( 'Alert', 'mega-blocks-gutenberg' ),
				'key' => 'alert'
			),
			array(
				'title' => __( 'Button', 'mega-blocks-gutenberg' ),
				'key' => 'button'
			),
			array(
				'title' => __( 'Panel', 'mega-blocks-gutenberg' ),
				'key' => 'panel'
			),
			array(
				'title' => __( 'Testimonial', 'mega-blocks-gutenberg' ),
				'key' => 'testimonial'
			),
			array(
				'title' => __( 'Profile Card', 'mega-blocks-gutenberg' ),
				'key' => 'profile-card'
			),
			array(
				'title' => __( 'Product Card', 'mega-blocks-gutenberg' ),
				'key' => 'product-card'
			),
			array(
				'title' => __( 'Count Up', 'mega-blocks-gutenberg' ),
				'key' => 'count-up'
			),
		);
	?>
	<table class="mbg-settings-table">
		<tr>
			<?php
				foreach ($block_data as $key => $b) { ?>
					<td>
						<label>
							<input type="checkbox"
							<?php echo (in_array($b['key'], $s_arr) || $all_checked) ? 'checked' : '' ; ?>
							name="block['<?php echo $b['key']; ?>']"
							value="<?php echo $b['key']; ?>">
							<?php echo $b['title']; ?>
						</label>
					</td>
				<?php
					echo (($key+1)%3 == 0) ? '</tr><tr>' : '' ;
				}
			?>
		</tr>
	</table>
	<p>
		<input type="submit" class="button button-primary save-settings" value="Save Changes">
	</p>
	</form>
</div>