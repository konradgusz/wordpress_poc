jQuery(document).ready(function($) {
	$('#mbg-settings-form').submit(function(e) {
	    e.preventDefault();
	    swal('Please Wait', "Saving settings...", 'info');
	    var data = $(this).serialize();
	    $.post(ajaxurl, data, function(resp) {
	        swal(resp.title, resp.message, resp.status);
	    }, 'json');
	});
});