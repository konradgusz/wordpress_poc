=== Mega UI Blocks for WordPress and Gutenberg ===
Contributors: Rameez_Iqbal
Tags: gutenberg, blocks, gutenberg blocks, mega blocks, editor, gutenberg components, custom blocks
Donate link: https://www.paypal.me/webcodingplace
Requires at least: 3.5
Tested up to: 5.1
Stable tag: 3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A collection of beautifully designed UI blocks for WordPress Editor.

== Description ==

Mega Blocks is a collection of beautifully designed content blocks for the new Block Editor in WordPress absolutely Free. With Mega Blocks, you will have much more control over each element making your web design significantly easier and faster.

Mega Blocks includes the Font Awesome 5 icons pack and Bootstrap 4 UI elements. Have suggestions for improving this free plugin? Please, provide them <a href="https://webcodingplace.com/contact-us/">here</a>.

<h2><b>MEGA BLOCKS INCLUDES THE FOLLOWING BLOCKS (AND ADDING MORE)</b></h2>
<ul>
	<li>Image Hover Effects Block - <a href="http://blocks.webcodingplace.com/image-hovers/">Image Hovers Demo</a></li>
	<li>UI Panels Block - <a href="http://blocks.webcodingplace.com/panels-cards/">Panels Demo</a></li>
	<li>User Profile Cards Block - <a href="http://blocks.webcodingplace.com/profile-cards/">Profile Cards Demo</a></li>
	<li>Icon Boxes - <a href="http://blocks.webcodingplace.com/icons/">Icon Boxes Demo</a></li>
	<li>Count Up Block - <a href="http://blocks.webcodingplace.com/counters/">Counters Demo</a></li>
	<li>Product Card Block - <a href="http://blocks.webcodingplace.com/product-cards/">Product Box Demo</a></li>
	<li>Alert Block - <a href="http://blocks.webcodingplace.com/alerts/">Alerts Demo</a></li>
	<li>Button Block - <a href="http://blocks.webcodingplace.com/buttons/">Buttons Demo</a></li>
	<li>List Group Block - <a href="http://blocks.webcodingplace.com/list-groups/">Listings Demo</a></li>
	<li>Testimonial Block - <a href="http://blocks.webcodingplace.com/testimonials/">Testimonials Demo</a></li>
	<li>Pricing Table Block - <a href="http://blocks.webcodingplace.com/pricing-table/">Pricing Table Demo</a></li>
</ul>

[youtube https://www.youtube.com/watch?v=LPr1D7e92ZE]

== Screenshots ==
1. Mega Blocks Category
2. Image Hover Block
3. Panel Block
4. Icon Block with Icons Picker
5. Settings to Enable/Disable blocks

== Changelog ==

= 3.0 =

* Icon Picker added
* Settings to enable/disable specific blocks
* other ui related bug fixes.

= 2.0 =

* Compatible and tested with WP 5.0

= 1.0 =

* Initial Release