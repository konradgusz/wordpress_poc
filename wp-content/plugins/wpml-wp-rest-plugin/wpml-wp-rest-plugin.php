<?php
/**
* Plugin Name: WPML WP REST PLUGIN
* Description:
* Version: 1.0.0
* Author: amanda
* License: GPL2+
*
* @package Wpml_Wp_Rest_Plugin
*/

if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly.
}

// Define constants.
define( 'WPML_WP_REST_PLUGIN_VERSION', '1.0.0' );
define( 'WPML_WP_REST_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

// Include the main class.
require plugin_dir_path( __FILE__ ) . 'includes/Wpml_Wp_Rest_Plugin.php';

// Main instance of plugin.
function rest_plugin() {
return Wpml_Wp_Rest_Plugin::get_instance();
}

// Global for backwards compatibility.
$GLOBALS['rest_plugin'] = rest_plugin();