<?php

class Wpml_Custom_Categories_Endpoint
{
    public function __construct()
    {
        $this->namespace = 'wpml-adapter/v1';
        $this->rest_base = 'custom-categories';
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, $this->rest_base, array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array($this, 'import_items'))
        ));
    }

    public function import_items($request)
    {

        $result = $this->insert_or_update_categories($request['data']);

        $data = rest_ensure_response($result);

        return $data;
    }

    private function insert_or_update_categories($items)
    {
        $response_data = array(
            'total' => 0,
            'processed' => 0,
            'inserted' => array(),
            'updated' => array()
        );

        foreach ($items as $key => $value):

            $response_data['total'] += count($value);
            $taxonomy = $key;

            foreach ($value as $item):

                $base = $item['base'];

                $result = $this->insert_or_update_category($base, $taxonomy);

                if ($result['new']) {
                    array_push($response_data['inserted'], $result['id']);
                } else {
                    array_push($response_data['updated'], $result['id']);
                }

                $original_category_id = $result['id'];

                $response_data['processed'] += 1;

                $translations = $item['translations'];
                $translated_categories_ids = array();

                foreach ($translations as $key => $value):

                    $result = $this->insert_or_update_category($value, $taxonomy);

                    if ($result['new']) {
                        array_push($response_data['inserted'], $result['id']);
                    } else {
                        array_push($response_data['updated'], $result['id']);
                    }

                    $translated_categories_ids[$key] = $result['id'];

                endforeach;

                $output = array(
                    'original' => $original_category_id,
                    'translations' => $translated_categories_ids
                );

                $this->connect_categories($output, $taxonomy);

            endforeach;

        endforeach;


        return $response_data;
    }

    private function insert_or_update_category($item, $taxonomy)
    {
        $exists = term_exists($item['id'], $taxonomy);

        if ($exists) {

            wp_update_term(
                $exists['term_id'],
                $taxonomy,
                array(
                    'name' => $item['name'],
                    'description' => $item['description']
                )
            );

            $id = (int)$exists['term_id'];

        } else {
            $term = wp_insert_term(
                $item['name'],
                $taxonomy,
                array(
                    'slug' => $item['id'],
                    'description' => $item['description']
                )
            );

            $id = $term['term_id'];
        }

        return array(
            'id' => $id,
            'new' => !$exists
        );
    }

    private function connect_categories($inserted_categories_ids, $taxonomy)
    {
        $wpml_element_type = apply_filters('wpml_element_type', $taxonomy);

        $original_category_lang = $this->get_lang_info($inserted_categories_ids['original'], $taxonomy);
        $translated_categories = $inserted_categories_ids['translations'];

        foreach ($translated_categories as $key => $value) {

            if (!$this->is_in_lang($value, $key, $taxonomy)) {
                $this->connect_translated($value, $key, $wpml_element_type, $original_category_lang);
            }
        }

    }

    private function connect_translated($id, $lang_code, $type, $original_category_lang_info)
    {
        $set_language_args = array(
            'element_id' => $id,
            'element_type' => $type,
            'trid' => $original_category_lang_info->trid,
            'language_code' => $lang_code,
            'source_language_code' => $original_category_lang_info->language_code
        );

        do_action('wpml_set_element_language_details', $set_language_args);
    }

    private function get_lang_info($id, $taxonomy)
    {
        $get_lang = array('element_id' => $id, 'element_type' => $taxonomy);
        $lang_info = apply_filters('wpml_element_language_details', null, $get_lang);

        return $lang_info;
    }

    private function is_in_lang($id, $lang_code, $taxonomy)
    {
        $lang_info = $this->get_lang_info($id, $taxonomy);

        return $lang_info->language_code == $lang_code;
    }
}