<?php

class Wpml_Custom_Post_Type_Endpoint_Factory
{

    public function __construct($type, $acf_keys, $acf_file_keys, $external_id)
    {
        $this->namespace = 'wpml-adapter/v1';
        $this->rest_base = $type;
        $this->post_type = $type;
        $this->acf_keys = $acf_keys;
        $this->acf_files_keys = $acf_file_keys;
        $this->external_id_key = $external_id;
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, $this->rest_base, array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array($this, 'import_items'))
        ));
    }

    public function import_items($request)
    {

        $result = $this->insert_or_update_posts($request['data']);

        $data = rest_ensure_response($result);

        return $data;
    }

    private function insert_or_update_posts($items)
    {
        $response_data = array(
            'total' => count($items),
            'processed' => 0,
            'inserted' => array(),
            'updated' => array()
        );

        foreach ($items as $item):

            $base_item = $item['base'];
            $result = $this->insert_or_update_post($base_item);
            $original_post_id = $result['id'];

            if ($original_post_id == 0) {
                continue;
            }

            $response_data['processed'] += 1;

            if ($result['new']) {
                array_push($response_data['inserted'], $original_post_id);
            } else {
                array_push($response_data['updated'], $original_post_id);
            }

            $translations = $item['translations'];
            $translated_posts_ids = array();


            foreach ($translations as $key => $value) {

                $result = $this->insert_or_update_post($value, $key);
                $translated_post_id = $result['id'];

                if ($translated_post_id == 0) {
                    continue;
                }

                if ($result['new']) {
                    array_push($response_data['inserted'], $translated_post_id);
                } else {
                    array_push($response_data['updated'], $translated_post_id);
                }

                $translated_posts_ids[$key] = $translated_post_id;
            }

            $output = array(
                'original' => $original_post_id,
                'translations' => $translated_posts_ids,
                'slug' => $base_item['slug']
            );

            $this->connect_posts($output);

        endforeach;

        return $response_data;
    }

    private function insert_or_update_post($item, $lang_code = 'en')
    {
        $args = array(
            'post_type' => $this->post_type,
            'meta_key' => $this->external_id_key,
            'meta_value' => $item[$this->external_id_key]
        );

        $old_posts = get_posts($args);

        $result = array_filter($old_posts, function ($post) use ($lang_code) {
            return $this->is_in_lang($post->ID, $lang_code);
        });

        if ($result[0]) {
            $id = $result[0]->ID;

        } else if ($result['1']) {
            $id = $result['1']->ID;

        } else {
            $id = 0;
        }

        if ($id != 0) {

            wp_update_post(array(
                'ID' => $id,
                'post_name' => $item['slug']
            ));

            $this->add_custom_fields($item, $id);

            return array(
                'id' => $id,
                'new' => false
            );
        }

        $post = $this->create_post($item['title'], $item['slug']);
        $post_id = wp_insert_post($post);
        $this->add_custom_fields($item, $post_id);

        return array(
            'id' => $post_id,
            'new' => true
        );
    }

    private function add_custom_fields($item, $post_id)
    {

        foreach ($this->acf_keys as $key) :

            $value = $item[$key];

            if (isset($value)) {

                if (in_array($key, $this->acf_files_keys, true)) {

                    $value = $this->upload_file($item[$key], $post_id);

                    if ($value == 0) {
                        return;
                    }
                }

                update_field($key, $value, $post_id);
            }

        endforeach;
    }


    private function connect_posts($inserted_posts_ids)
    {
        $wpml_element_type = apply_filters('wpml_element_type', $this->post_type);

        $original_post_language_info = $this->get_lang_info($inserted_posts_ids['original']);
        $translated_posts = $inserted_posts_ids['translations'];

        foreach ($translated_posts as $key => $value) {

            if (!$this->is_in_lang($value, $key)) {

                $this->connect_translated($value, $key, $wpml_element_type, $original_post_language_info);

                wp_update_post(array(
                    'ID' => $value,
                    'post_name' => $inserted_posts_ids['slug']
                ));
            }
        }

    }

    private function connect_translated($id, $lang_code, $type, $original_post_language_info)
    {
        $set_language_args = array(
            'element_id' => $id,
            'element_type' => $type,
            'trid' => $original_post_language_info->trid,
            'language_code' => $lang_code,
            'source_language_code' => $original_post_language_info->language_code
        );

        do_action('wpml_set_element_language_details', $set_language_args);
    }

    private function get_lang_info($id)
    {
        $get_lang = array('element_id' => $id, 'element_type' => $this->post_type);
        $lang_info = apply_filters('wpml_element_language_details', null, $get_lang);

        return $lang_info;
    }

    private function is_in_lang($id, $lang_code)
    {
        $lang_info = $this->get_lang_info($id);

        return $lang_info->language_code == $lang_code;
    }

    private function create_post($title, $name)
    {
        return array(
            'post_type' => $this->post_type,
            'post_title' => $title,
            'post_name' => $name,
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
        );
    }

    private function create_attachment($type, $name)
    {
        return array(
            'post_mime_type' => $type,
            'post_title' => sanitize_file_name($name),
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
        );
    }

    private function upload_file($image_url)
    {
        $content = $this->get_remote_content($image_url);

        if ($content == 0) {
            return 0;
        }

        $content_type = $content['headers']['Content-Type'];
        $content_dispositon = $content['headers']['Content-Disposition'];

        $filetype = $this->get_filetype($content_type);
        $filename = $this->get_filename($content_dispositon);

        $old_image = get_page_by_title(sanitize_file_name($filename), OBJECT, 'attachment');

        if ($old_image) {

            $attach_id = $old_image->ID;

        } else {

            $uploaddir = wp_upload_dir();
            $uploadfile = $uploaddir['path'] . '/' . $filename;

            $savefile = fopen($uploadfile, 'w');
            fwrite($savefile, $content['data']);
            fclose($savefile);

            $attachment = $this->create_attachment($filetype, $filename);
            $attach_id = wp_insert_attachment($attachment, $uploadfile);
        }

        return $attach_id;
    }

    private function get_filetype($string)
    {
        return substr($string, 0, strpos($string, ';'));

    }

    private function get_filename($string)
    {
        return substr($string, strpos($string, '=') + 2, -1);
    }

    private function get_remote_content($url)
    {
        $response = wp_remote_get($url,
            array()
        );

        if (is_wp_error($response)) {
            return 0;
        }

        $data = wp_remote_retrieve_body($response);
        $headers = wp_remote_retrieve_headers($response);

        if (empty($data)) {
            return 0;
        }

        return array(
            'data' => $data,
            'headers' => $headers
        );
    }
}