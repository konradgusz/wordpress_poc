<?php
/**
 * Main class
 *
 * @package Wpml_Wp_Rest_Plugin
 *
 */

class Wpml_Wp_Rest_Plugin
{

    public static function get_instance()
    {
        static $instance = null;
        if (is_null($instance)) {
            $instance = new self();
        }
        return $instance;
    }

    private function __construct()
    {
        $this->includes();
        $this->cinema_acf_keys = array(
            'cinema_photo',
            'contact_email',
            'longitude',
            'latitude',
            'address_section',
            'reservations_section',
            'transport_section',
            'screens_ticketing',
            'block_sales',
            'prices_file',
            'snacks_and_drinks',
            'accessibility',
            'facilities_for_families',
            'description',
            'cinema_name',
            'export_code'
        );
        $this->cinema_acf_file_keys = array(
            'cinema_photo',
            'prices_file'
        );
        $this->cinema_external_id = 'export_code';

        $this->movie_acf_keys = array(
            'cinema_photo',
            'contact_email',
            'longitude',
            'latitude',
            'address_section',
            'reservations_section',
            'transport_section',
            'screens_ticketing',
            'block_sales',
            'prices_file',
            'snacks_and_drinks',
            'accessibility',
            'facilities_for_families',
            'description',
            'cinema_name',
            'export_code'
        );
        $this->movie_acf_file_keys = array(
            'cinema_photo',
            'prices_file'
        );
        $this->movie_external_id = 'export_code';
    }

    public function includes()
    {
        require plugin_dir_path(__FILE__) . 'Wpml_Custom_Post_Type_Endpoint_Factory.php';
        require plugin_dir_path(__FILE__) . 'Wpml_Custom_Categories_Endpoint.php';


        add_action('rest_api_init', function () {
            $controller = new Wpml_Custom_Post_Type_Endpoint_Factory('cinemas', $this->cinema_acf_keys, $this->cinema_acf_file_keys, $this->cinema_external_id);
            $controller->register_routes();
        });

        add_action('rest_api_init', function () {
            $controller = new Wpml_Custom_Post_Type_Endpoint_Factory('movies', $this->movie_acf_keys, $this->movie_acf_file_keys, $this->movie_external_id);
            $controller->register_routes();
        });

        add_action('rest_api_init', function () {
            $controller = new Wpml_Custom_Categories_Endpoint();
            $controller->register_routes();
        });
    }

}