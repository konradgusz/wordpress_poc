<?php
function get_main_menu() {
    return wp_get_nav_menu_items('main-menu');
}

function get_footer_menu() {
    return wp_get_nav_menu_items('footer');
}

function get_wpml_languages() {
    return apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/main-menu', array(
        'methods' => 'GET',
        'callback' => 'get_main_menu',
    ));
    register_rest_route('wp/v2', '/footer-menu', array(
        'methods' => 'GET',
        'callback' => 'get_footer_menu',
    ));
    register_rest_route('wpml-adapter/v1', '/active-languages', array(
        'methods' => 'GET',
        'callback' => 'get_wpml_languages',
    ));
});
?>