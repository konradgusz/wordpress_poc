<?php
function cptui_register_my_cpts() {

	/**
	 * Post Type: Cinemas.
	 */

	$labels = array(
		"name" => __( "Cinemas", "website-theme" ),
		"singular_name" => __( "Cinema", "website-theme" ),
		"menu_name" => __( "Cinemas", "website-theme" ),
		"all_items" => __( "All Cinemas", "website-theme" ),
		"add_new" => __( "Add Cinema", "website-theme" ),
		"add_new_item" => __( "Add Cinema", "website-theme" ),
		"edit_item" => __( "Edit Cinema", "website-theme" ),
		"new_item" => __( "New Cinema", "website-theme" ),
		"view_item" => __( "View Cinema", "website-theme" ),
		"view_items" => __( "View Cinemas", "website-theme" ),
		"search_items" => __( "Search Cinema", "website-theme" ),
		"not_found" => __( "No Cinemas Found", "website-theme" ),
		"not_found_in_trash" => __( "No Cinemas Found in Trash", "website-theme" ),
		"parent_item_colon" => __( "Parent Cinemas", "website-theme" ),
		"featured_image" => __( "Featured Image", "website-theme" ),
		"set_featured_image" => __( "Set featured Image", "website-theme" ),
		"remove_featured_image" => __( "Remove Featured Image", "website-theme" ),
		"use_featured_image" => __( "Use Featured Image", "website-theme" ),
		"archives" => __( "Cinemas Archive", "website-theme" ),
		"insert_into_item" => __( "Insert into cinema", "website-theme" ),
		"uploaded_to_this_item" => __( "Upload to this cinema", "website-theme" ),
		"filter_items_list" => __( "Filter cinemas list", "website-theme" ),
		"items_list_navigation" => __( "Cinemas list navigation", "website-theme" ),
		"items_list" => __( "Cinemas list", "website-theme" ),
		"attributes" => __( "Cinemas attributes", "website-theme" ),
		"name_admin_bar" => __( "Cinema", "website-theme" ),
		"parent_item_colon" => __( "Parent Cinemas", "website-theme" ),
	);

	$args = array(
		"label" => __( "Cinemas", "website-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "cinemas", "with_front" => false ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail", "custom-fields", "revisions", "page-attributes" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "cinemas", $args );

	/**
	 * Post Type: Movies.
	 */

	$labels = array(
		"name" => __( "Movies", "website-theme" ),
		"singular_name" => __( "Movie", "website-theme" ),
		"menu_name" => __( "Movies", "website-theme" ),
		"all_items" => __( "All Movies", "website-theme" ),
		"add_new" => __( "Add Movie", "website-theme" ),
		"add_new_item" => __( "Add Movie", "website-theme" ),
		"edit_item" => __( "Edit Movie", "website-theme" ),
		"new_item" => __( "New Movie", "website-theme" ),
		"view_item" => __( "View Movie", "website-theme" ),
		"view_items" => __( "View Movies", "website-theme" ),
		"search_items" => __( "Search Movie", "website-theme" ),
		"not_found" => __( "No Movies Found", "website-theme" ),
		"not_found_in_trash" => __( "No Movies Found in Trash", "website-theme" ),
		"parent_item_colon" => __( "Parent Movies", "website-theme" ),
		"featured_image" => __( "Featured Image", "website-theme" ),
		"set_featured_image" => __( "Set featured Image", "website-theme" ),
		"remove_featured_image" => __( "Remove Featured Image", "website-theme" ),
		"use_featured_image" => __( "Use Featured Image", "website-theme" ),
		"archives" => __( "Movies Archive", "website-theme" ),
		"insert_into_item" => __( "Insert into movie", "website-theme" ),
		"uploaded_to_this_item" => __( "Upload to this movie", "website-theme" ),
		"filter_items_list" => __( "Filter movies list", "website-theme" ),
		"items_list_navigation" => __( "Movies list navigation", "website-theme" ),
		"items_list" => __( "Movies list", "website-theme" ),
		"attributes" => __( "Movies attributes", "website-theme" ),
		"name_admin_bar" => __( "Movie", "website-theme" ),
		"parent_item_colon" => __( "Parent Movies", "website-theme" ),
	);

	$args = array(
		"label" => __( "Movies", "website-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "movies", "with_front" => false ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail", "custom-fields", "revisions", "page-attributes" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "movies", $args );

	/**
	 * Post Type: Hero banners.
	 */

	$labels = array(
		"name" => __( "Hero banners", "website-theme" ),
		"singular_name" => __( "Hero banner", "website-theme" ),
		"menu_name" => __( "Hero banners", "website-theme" ),
		"all_items" => __( "All hero banners", "website-theme" ),
		"add_new" => __( "Add new hero banner", "website-theme" ),
		"add_new_item" => __( "Add new hero banner", "website-theme" ),
		"edit_item" => __( "Edit hero banner", "website-theme" ),
		"new_item" => __( "New hero banner", "website-theme" ),
		"view_item" => __( "View hero banner", "website-theme" ),
		"view_items" => __( "View hero banners", "website-theme" ),
		"search_items" => __( "Search hero banner", "website-theme" ),
		"not_found" => __( "No hero banner found", "website-theme" ),
		"not_found_in_trash" => __( "No hero banners found in trash", "website-theme" ),
	);

	$args = array(
		"label" => __( "Hero banners", "website-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "hero_banners", "with_front" => false ),
		"query_var" => true,
		"supports" => array( "title", "custom-fields" ),
	);

	register_post_type( "hero_banners", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
?>