<?php
function cptui_register_my_taxes() {

    /**
     * Taxonomy: Age restrictions.
     */

    $labels = array(
        "name" => __( "Age restrictions", "website-theme" ),
        "singular_name" => __( "Age restriction", "website-theme" )
    );

    $args = array(
        "label" => __( "Age restrictions", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'age_restriction', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "age_restriction",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "age_restriction", array( "movies" ), $args );

    /**
     * Taxonomy: Genres.
     */

    $labels = array(
        "name" => __( "Genres", "website-theme" ),
        "singular_name" => __( "Genre", "website-theme" ),
    );

    $args = array(
        "label" => __( "Genres", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'genres', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "genres",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "genres", array( "movies" ), $args );

    /**
     * Taxonomy: Screening addons.
     */

    $labels = array(
        "name" => __( "Screening addons", "website-theme" ),
        "singular_name" => __( "Screening addon", "website-theme" ),
    );

    $args = array(
        "label" => __( "Screening addons", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'screening_addons', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "screening_addons",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "screening_addons", array( "cinemas", "movies" ), $args );

    /**
     * Taxonomy: Screening types.
     */

    $labels = array(
        "name" => __( "Screening types", "website-theme" ),
        "singular_name" => __( "Screening type", "website-theme" ),
    );

    $args = array(
        "label" => __( "Screening types", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'screening_type', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "screening_type",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "screening_type", array( "cinemas", "movies" ), $args );

    /**
     * Taxonomy: Special types.
     */

    $labels = array(
        "name" => __( "Special types", "website-theme" ),
        "singular_name" => __( "Special type", "website-theme" ),
    );

    $args = array(
        "label" => __( "Special types", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'special_type', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "special_type",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "special_type", array( "cinemas", "movies" ), $args );

    /**
     * Taxonomy: Extra screening types.
     */

    $labels = array(
        "name" => __( "Extra screening types", "website-theme" ),
        "singular_name" => __( "Extra screening type", "website-theme" ),
    );

    $args = array(
        "label" => __( "Extra screening types", "website-theme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => false,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'extra', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "extra",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "extra", array( "cinemas", "movies" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes' );

?>