<?php
//Child Theme Functions File

add_action('wp_enqueue_scripts', 'enqueue_wp_child_theme');

function enqueue_wp_child_theme()
{
    wp_enqueue_style('parent-css', get_template_directory_uri() . '/style.css');

    wp_enqueue_style('child-css', get_stylesheet_uri());

    wp_enqueue_script('child-js', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0', true);
}

function wpse_remove_parent_theme_locations()
{
    // @link http://codex.wordpress.org/Function_Reference/unregister_nav_menu
    unregister_nav_menu('menu-1');
    unregister_nav_menu('footer');
    unregister_nav_menu('social');
}

register_nav_menus(
    array(
        'main-menu' => __('Primary', 'website'),
        'footer' => __('Footer Menu', 'website'),
    )
);


if (function_exists('acf_add_options_page')) {

    acf_add_options_page([
        'page_title' => 'Network Options',
        'menu_title' => 'Network Options',
        'position' => 0,
        'post_id' => 'network_options',
        'autoload' => true,
        'redirect' => false
    ]);

    acf_add_options_page([
        'page_title' => 'Config',
        'menu_title' => 'Config',
        'position' => 999,
        'post_id' => 'options',
        'autoload' => true

    ]);

}
include 'custom-post-types.php';
include 'custom-taxonomies.php';
include 'custom-fields.php';
//include 'custom-filters.php';
include 'custom-image-sizes.php';
include 'add-post-query-by-meta-value-in-rest-api.php';
include 'rest-routes.php';
// TODO: custom categories import to endpoint so it can be called on demand
include 'custom-categories.php';

add_action('after_setup_theme', 'wpse_remove_parent_theme_locations', 20);

?>