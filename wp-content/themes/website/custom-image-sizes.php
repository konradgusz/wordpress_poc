<?php

add_theme_support( 'post-thumbnails' );
add_image_size( 'poster-large', 600, 889 );
add_image_size( 'poster-medium', 220, 326 );

?>